\contentsline {section}{\numberline {1}Introduction}{1}{section.1}%
\contentsline {section}{\numberline {2}Discrete Regularity Structures}{6}{section.2}%
\contentsline {subsection}{\numberline {2.1}Discrete Models}{7}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Renormalised Model}{11}{subsection.2.2}%
\contentsline {section}{\numberline {3}Labelled Graphs}{15}{section.3}%
\contentsline {subsection}{\numberline {3.1}Directed MultiGraph and Contracted Graphs}{15}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Partition of Integral Domain}{18}{subsection.3.2}%
\contentsline {section}{\numberline {4}Elementary Labelled Graphs}{23}{section.4}%
\contentsline {subsection}{\numberline {4.1}Construction of Elementary Labelled Graphs}{24}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Bounds on Elementary Labelled Graphs}{26}{subsection.4.2}%
\contentsline {section}{\numberline {5}Solving the Generalised KPZ Equation}{29}{section.5}%
\contentsline {subsection}{\numberline {5.1}Bounds on the Discrete Model}{29}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}General Criterion}{32}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Renormalisation Procedure}{34}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Example Computations}{39}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Convergence of the model}{47}{subsection.5.5}%
\contentsline {section}{\numberline {A}Joint Cumulants}{48}{appendix.A}%
\contentsline {section}{Acknowledgments}{49}{section*.5}%
\contentsline {section}{References}{49}{section*.7}%
