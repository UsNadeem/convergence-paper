%% Template for the submission to:
%%   The Annals of Probability [AOP]
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% In this template, the places where you   %%
%% need to fill in your information are     %%
%% indicated by '???'.                      %%
%%                                          %%
%% Please do not use \input{...} to include %%
%% other tex files. Submit your LaTeX       %%
%% manuscript as one .tex document.         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[aop]{imsart}

%% Packages
\RequirePackage{amsthm,amsmath,amsfonts,amssymb}
\RequirePackage[numbers]{natbib}
\usepackage{breakurl}
\usepackage{hyperref}
\usepackage[cal=boondoxo]{mathalfa}
%\RequirePackage[authoryear]{natbib}%% uncomment this for author-year citations
%\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}%% uncomment this for coloring bibliography citations and linked URLs
%\RequirePackage{graphicx}%% uncomment this for including figures

\startlocaldefs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                          %%
%% Uncomment next line to change            %%
%% the type of equation numbering           %%
%%                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\numberwithin{equation}{section}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                          %%
%% For Axiom, Claim, Corollary, Hypothesis, %%
%% Lemma, Theorem, Proposition              %%
%% use \theoremstyle{plain}                 %%
%%                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\theoremstyle{plain}
%\newtheorem{???}{???}
%\newtheorem*{???}{???}
%\newtheorem{???}{???}[???]
%\newtheorem{???}[???]{???}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                          %%
%% For Assumption, Definition, Example,     %%
%% Notation, Property, Remark, Fact         %%
%% use \theoremstyle{remark}                %%
%%                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\theoremstyle{remark}
%\newtheorem{???}{???}
%\newtheorem*{???}{???}
%\newtheorem{???}{???}[???]
%\newtheorem{???}[???]{???}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Please put your definitions here:        %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\endlocaldefs

\begin{document}

\begin{frontmatter}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                          %%
%% Enter the title of your article here     %%
%%                                          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Convergence of space-discretised gKPZ via Regularity Structures}
%\title{A sample article title with some additional note\thanksref{T1}}
\runtitle{???}
%\thankstext{T1}{A sample of additional note to the title.}

\begin{aug}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Only one address is permitted per author. %%
%% Only division, organization and e-mail is %%
%% included in the address.                  %%
%% Additional information can be included in %%
%% the Acknowledgments section if necessary. %%
%% ORCID can be inserted by command:         %%
%% \orcid{0000-0000-0000-0000}               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\author[A]{\fnms{Yvain}~\snm{Bruned}\ead[label=e1]{yvain.bruned@univ-lorraine.fr}},
\author[B]{\fnms{Usama}~\snm{Nadeem}\ead[label=e2]{M.U.Nadeem@sms.ed.ac.uk}}
\and
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Addresses                                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\address[A]{IECL (UMR 7502), Université de Lorraine\printead[presep={,\ }]{e1}}

\address[B]{University of Edinburgh\printead[presep={,\ }]{e2}}
\end{aug}

\begin{abstract}
In this work, we show a convergence result for the discrete formulation of the generalised KPZ equation $\partial_t u = (\Delta u) + g(u)(\nabla u)^2 + k(\nabla u) + h(u) + f(u)\xi_t(x)$, where the $\xi$ is a real-valued random field, $\Delta$ is the discrete Laplacian, and $\nabla$ is a discrete gradient, without fixing the spatial dimension. Our convergence result is established within the discrete regularity structures introduced by Hairer and Erhard \cite{EH17}. We extend with new ideas the convergence result found in \cite{MH21} that deals with a discrete form of the Parabolic Anderson model driven by a (rescaled) symmetric simple exclusion process. This is the first time that a discrete generalised KPZ equation is treated and it is a major step toward a general convergence result that will cover a large family of discrete models.
\end{abstract}

\begin{keyword}[class=MSC]
\kwd[Primary ]{60L30}
\kwd{60L90}
\kwd[; secondary ]{60H15}
\end{keyword}

\begin{keyword}
\kwd{Discrete models}
\kwd{Generalised KPZ equation}
\kwd{Regularity Structures}
\kwd{Stochastic PDE}
\end{keyword}

\end{frontmatter}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Please use \tableofcontents for articles %%
%% with 50 pages and more                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Main text entry area:

\section{Introduction}

Martin Hairer's theory of Regularity Structures \cite{reg} has been applied to construct a solution theory for a large subclass of singular stochastic PDEs of the form:

\beq\label{eq:sSPDE}
\partial_t u - \mathcal{L}u = F(u,\nabla u,\xi)
\eeq

where $\mathcal{L}$ is some differential operator and $F$ is some non-linearity affine in the noise $ \xi $. Central to this solution theory is the idea of renormalisation which usually involves subtracting certain counterterms from the equation so as to deal with products of distributions that are undefined in the classical sense. 

Since the original paper \cite{reg}, the programme for generating the solution theory for a given SPDE has been automated. In \cite{BHZ}, the authors explained how to extract \textit{rules} from a given equation, and then proceduralised the construction of a renormalisation group that can affect renormalisation on the equation. In this programme, the choice of the aforementioned counter-terms comes from the classic BPHZ formalism \cite{BP57,KH69,WZ69}. In \cite{CH16}, the authors provide a functionally black-box like result that automatically produces the required stochastic estimates for the renormalised stochastic objects coming from \cite{BHZ}. The last step completed in \cite{BCCH} was to write the fixed point and the action of the renormalisation onto the right hand side of \eqref{eq:sSPDE}. Hence, the programme in the continuum has been automated by this series of papers. 

Many equations of the form \eqref{eq:sSPDE}, arise from scaling limits of the microscopic models - consider \cite{BG97,MW17,K16} for derivations of Stochastic Burgers, KPZ, $\Phi^4$ and the parabolic Anderson model - and as such there is interest in discretisations of these equations. A classic example is from \cite{BG97}, where employing an approach based on the Cole-Hopf transform, the authors proved that a (rescaled) particle system converges to their notion of the solution of the KPZ. In \cite{GJ10,GJ14}, the authors introduced a martingale type approach to the problem - the so-called energy solution. Further, they showed the limits of a sizeable class of suitably scaled interacting particle systems are also energy solutions. The uniqueness of the energy solution has been proved in \cite{GP18} which implies various applications given in \cite{GP16,DGP17}. These approaches that we have listed thus far have certain drawbacks -  for the former the difficulty is that the Hopf-Cole transform is difficult to implement at a discrete level, and for the latter, knowledge and control of the invariant measure for the system is a requisite.

The use of regularity structures circumvents both of these problems and hence has been shown to have great promise for discrete problems such as these. In \cite{HM18}, the authors developed a framework to adapt the theory of regularity structure for certain spatial discretisations. Further generality was achieved in \cite{EH17}, where the authors do not fix any particular discretisation procedure. As applications to the discrete regularity structures constructed in these papers, in \cite{HM18} a space-discretised KPZ was proven to converge to the solution of the generic KPZ, and then in \cite{CM18} the same was proven for a space-time discretisation of the KPZ equation. Convergence of more general discretisations driven by interacting particle systems to stochastic PDEs in paper such as \cite{MH21} and \cite{Mat18} was addressed. In the former, the author used multiple stochastic integrals with respect to martingales with particular properties that allow for certain moment bounds and expansions that are analogous to the Nelson's Estimate and Wiener chaos expansions that are used in \cite{reg} in the case of (homogeneous) Gaussian Noise. In the latter, the authors begin with a discrete parabolic Anderson model driven by a symmetric simple exclusion process and show that the solutions to a suitably renormalised version of the equation converge in law to the solution of the same equation driven by a generalised Ornstein-Uhlenbeck process. This is achieved via estimates on joint cumulants of arbitrary large order for the exclusion process. 
 
Another reason that drives interest in discrete regularity structures is that it may possibly provide a way to link invariant measures to their dynamics. See \cite{BGHZ22} for a conjecture connecting the Brownian loop measure to stochastic geometric heat equations, that falls short of being a theorem due to the lack of a convergence result in the way of \cite{CH16} for discrete regularity structure. In \cite{C22, CCHS20, CCHS22} the authors study the stochastic quantisation for the Yang-Mills in the 2 and 3 dimensional Euclidean space and they have the same type of open problem for the invariant measure.

Apart from Regularity Structures, other theories have been successful in treating singular SPDEs. One such is via the use of paracontrolled calculus as in \cite{GIP13}, which much in the way of Hairer, describes the spectral features of a function in terms of paracontrolled distributions. This methodology has also been used to look at discrete problems. In \cite{ZZ14}, for example, the author use the theory to approximate the Navier-Stokes equation, while in \cite{ZZ15} the authors construct a piecewise linear approximation for the $\Phi^4_3$ model. In \cite{GP17}, the authors achieve a similar convergence result as we present here, for Sasamoto-Spohn type discretisations of the stochastic Burgers equation, in that they show that the limit of such discretisations solves (a version of) the continuous Burgers equations. In \cite{CGP16}, the authors also look at the discrete PAM, but the tool for proving convergence is paracontrolled calculus and finally in \cite{MP19} the authors develop a discrete version of paracontrolled distributions.

In this paper, we intend to extend the work done in \cite{MH21} by presenting a renormalisation procedure that is able to handle more complicated equations. We fix the following discretisation of the generalised KPZ equation:
\begin{equs}\label{eq:gKPZ}\tag{gKPZ}
\partial_t u = \Delta u + g(u)(\nabla u)^2+k(u) \nabla u+h(u)+f(u)\xi_t(x) 
\end{equs}
to showcase our methodology. Here $t\ge 0$, $x\in\mathbb{Z}^d$, $\nabla$ is a discrete gradient and $\Delta$ is the discrete Laplacian defined:
\begin{equs}
\Delta u(t,x) = \sum_{y:\,x\sim y} [u(y,t) - u(x,t)]
\end{equs} 

where $x\sim y$ means that $x$ and $y$ are nearest neighbours with respect to the Euclidean norm on $\mathbb{Z}^d$ and $\xi$ is a $\mathbb{R}$-valued random field which we will not fix although we do assume that it is centred.

As in \cite{MH21} we would like to prove that the solution of \eqref{eq:gKPZ} converges, in some sense to be specified later, to the solution $\bar u$ of the gKPZ equation driven by some space-time random field $Y$ defined on $\mb{R}_+\times\mb{R}^d$:
\begin{equs}\label{eq:gKPZcon}
\partial_t \bar u = \Delta \bar u + g(\bar u)(\nabla \bar u)^2+k(\bar u) \nabla \bar u+h(\bar u)+f(\bar u)Y_t(x)
\end{equs}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Single Appendix:                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{appendix}
%\section*{???}%% if no title is needed, leave empty \section*{}.
%\end{appendix}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Multiple Appendixes:                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{appendix}
%\section{???}
%
%\section{???}
%
%\end{appendix}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Support information, if any,             %%
%% should be provided in the                %%
%% Acknowledgements section.                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{acks}[Acknowledgments]
% The authors would like to thank ...
%\end{acks}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Funding information, if any,             %%
%% should be provided in the                %%
%% funding section.                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{funding}
% The first author was supported by ...
%
% The second author was supported in part by ...
%\end{funding}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Supplementary Material, including data   %%
%% sets and code, should be provided in     %%
%% {supplement} environment with title      %%
%% and short description. It cannot be      %%
%% available exclusively as external link.  %%
%% All Supplementary Material must be       %%
%% available to the reader on Project       %%
%% Euclid with the published article.       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{supplement}
%\stitle{???}
%\sdescription{???.}
%\end{supplement}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                  The Bibliography                       %%
%%                                                         %%
%%  imsart-???.bst  will be used to                        %%
%%  create a .BBL file for submission.                     %%
%%                                                         %%
%%  Note that the displayed Bibliography will not          %%
%%  necessarily be rendered by Latex exactly as specified  %%
%%  in the online Instructions for Authors.                %%
%%                                                         %%
%%  MR numbers will be added by VTeX.                      %%
%%                                                         %%
%%  Use \cite{...} to cite references in text.             %%
%%                                                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% if your bibliography is in bibtex format, uncomment commands:
%\bibliographystyle{imsart-number} % Style BST file (imsart-number.bst or imsart-nameyear.bst)
%\bibliography{bibliography}       % Bibliography file (usually '*.bib')

%% or include bibliography directly:
% \begin{thebibliography}{}
% \bibitem{b1}
% \end{thebibliography}

\begin{thebibliography}{99}
\bibitem{BB21}
I.~Bailleul, Y.~{Bruned}.
\newblock { \em Renormalised singular stochastic PDEs}. 
 \burlalt{arXiv:2101.11949}{http://arxiv.org/abs/2101.11949}. 
 
 \bibitem{BB21b}
I.~Bailleul, Y.~{Bruned}.
\newblock { \em  Locality for singular stochastic PDEs}. 
 \burlalt{arXiv:2109.00399}{https://arxiv.org/abs/2109.00399}.


\bibitem{BCCH}
 { \rm Y. Bruned, A. Chandra, I. Chevyrev,
  M. Hairer}.
\newblock {\em Renormalising SPDEs in regularity structures}.
\newblock J. Eur. Math. Soc. (JEMS), \textbf{23}, no.~3, (2021), 869--947.
\newblock
  \burlalt{doi:10.4171/JEMS/1025}{http://dx.doi.org/10.4171/JEMS/1025}.
  
  \bibitem{BG97}
L. ~Bertini, G. ~Giacomin.
\newblock {\em Stochastic Burgers and KPZ equations from particle systems.}
\newblock Comm. Math. Phys. \textbf{183}, no.~3, (1997), 571--607.
\newblock \burlalt{doi:10.1007/s002200050044}{https://doi.org/10.1007/s002200050044}.
 
 \bibitem{BGHZ22}
Y.~{Bruned}, F.~{Gabriel}, M.~{Hairer},
  L.~{Zambotti}.
\newblock \emph{Geometric stochastic heat equations}.
\newblock J. Amer. Math. Soc. (JAMS), \textbf{35}, no.~1, (2022),
  1--80.
\newblock \burlalt{doi:10.1090/jams/977}{http://dx.doi.org/10.1090/jams/977}. 


  
\bibitem{BHZ}
{\rm Y. Bruned, M. Hairer, L. Zambotti}.
\newblock {\em Algebraic renormalisation of regularity structures.}
\newblock Invent. Math. \textbf{215}, no.~3, (2019), 1039--1156.
\newblock
  \burlalt{doi:10.1007/s00222-018-0841-x}{https://dx.doi.org/10.1007/s00222-018-0841-x}.

\bibitem{EMS}
  {\rm Y. Bruned, M. Hairer, L. Zambotti}.
\newblock {\em Renormalisation of Stochastic Partial Differential Equations.}
\newblock EMS Newsletter \textbf{115}, no.~3, (2020), 7--11.
\newblock
  \burlalt{doi: 10.4171/NEWS/115/3}{http://dx.doi.org/10.4171/NEWS/115/3}.
   
\bibitem{BP57}
N.~N. Bogoliubow, O.~S. Parasiuk.
\newblock { \em \"{U}ber die {M}ultiplikation der {K}ausalfunktionen in der
  {Q}uantentheorie der {F}elder.}
\newblock Acta Math. \textbf{97}, (1957), 227--266.
\newblock
  \burlalt{doi:10.1007/BF02392399}{http://dx.doi.org/10.1007/BF02392399}.

\bibitem{BR18} 
{ \rm Y. Bruned}.
\newblock {\em Recursive formulae in regularity structures.}
\newblock Stoch. Partial Differ. Equ. Anal. and Comput. \textbf{6},
  no.~4, (2018), 525--564.
\newblock 
  \burlalt{doi:10.1007/s40072-018-0115-z}{http://dx.doi.org/10.1007/s40072-018-0115-z}.   
  

\bibitem{CCHS20}
A. ~Chandra, I. ~Chevyrev, M. ~Hairer, H. ~Shen.
\newblock {\em Langevin dynamic for the 2D Yang-Mills measure}.
Publ. math. IHES, (2022).
\newblock \burlalt{doi:10.1007/s10240-022-00132-0}{https://doi.org/10.1007/s10240-022-00132-0}.

\bibitem{CCHS22}
A. ~Chandra, I. ~Chevyrev, M. ~Hairer, H. ~Shen.
\newblock {\em Stochastic quantisation of Yang-Mills-Higgs in 3D}.
\newblock \burlalt{arXiv:2201.03487v1}{https://arxiv.org/abs/2201.03487v1}.

\bibitem{CGP16}
K.~Chouk, J.~Gairing, N. ~Perkowski.
\newblock{\em An invariance principle
for the two-dimensional parabolic Anderson model with small potential}.
\newblock {Stoch. Partial Differ. Equ. Anal. and Comput. \textbf{5}, (2017).}
\newblock \burlalt{doi:10.1007/s40072-017-0096-3}{https://doi.org/10.1007/s40072-017-0096-3}.

\bibitem{CH16}
A.~Chandra, M.~Hairer.
\newblock {\em An analytic {BPHZ} theorem for regularity structures}.
\newblock \burlalt{arXiv:1612.08138}{http://arxiv.org/abs/1612.08138}. 

\bibitem{C22}
I. ~Chevyrev.
\newblock {\em Stochastic quantisation of Yang-Mills}.
\newblock \burlalt{arXiv:2202.13359}{https://arxiv.org/abs/2202.13359}.

\bibitem{CM18}
G.~Cannizzaro, K.~Matetski.
\newblock {\em Space-time discrete KPZ equation}.
Comm. Math. Phys. \textbf{358},
(2018), 521–588. 
\newblock \burlalt{doi:10.1007/s00220-018-3089-9}{https://doi.org/10.1007/s00220-018-3089-9}.

 

\bibitem{DGP17}
J.~Diehl, M.~Gubinelli, N.~Perkowski. 
\newblock {\em The Kardar–Parisi–Zhang Equation as Scaling Limit of Weakly Asymmetric Interacting Brownian Motions}.
Comm. Math. Phys. \textbf{354},
(2017), 549–589. 
\newblock \burlalt{doi:10.1007/s00220-017-2918-6}{https://doi.org/10.1007/s00220-017-2918-6}.

\bibitem{EH17}
{\rm D. Erhard, M. Hairer}
\newblock {\em Discretisation of regularity structures.}
\newblock {Ann. Inst. H. Poincaré Probab. Statist. \textbf{55}, no.~4, (2019), 2209--2248.}
\newblock \burlalt{doi:10.1214/18-AIHP947}{https://doi.org/10.1214/18-AIHP947}.

\bibitem{MH21}
D.~Erhard, M.~Hairer.
\newblock {\em A scaling limit of the parabolic Anderson model with exclusion interaction}.
\newblock \burlalt{arXiv:2103.13479}{https://arxiv.org/abs/2103.13479}.

\bibitem{GIP13}
M. ~Gubinelli, P. ~Imkeller, N. ~Perkowski.
\newblock {\em Paracontrolled distributions and singular PDEs}.
\newblock{Forum of Mathematics Pi, \textbf{3}, e6, (2015)}.
\newblock \burlalt{doi:1017/fmp.2015.2}{https://doi.org/10.1017/fmp.2015.2}.

\bibitem{GJ10}
P. ~Gon\c{c}alves, M. ~Jara.
\newblock {\em Universality of KPZ equation}.
\newblock arXiv preprint, (2010).
\newblock \burlalt{arxiv:1003.
4478}{http://arxiv.org/abs/1003.
4478}.

\bibitem{GJ14}
P. ~Gon\c{c}alves, M. ~Jara.
\newblock {\em Nonlinear fluctuations of weakly asymmetric interacting particle systems}.
\newblock Arch. Ration. Mech. Anal. \textbf{212}, no.~2, (2014), 597–-644.
\newblock \burlalt{doi:10.1007/
s00205-013-0693-x.
58}{http://dx.doi.org/10.1007/
s00205-013-0693-x.
58}.



\bibitem{GP16}
M. ~Gubinelli, N. ~Perkowski.
\newblock {\em The Hairer-Quastel universality result at stationarity}.
\newblock  Stochastic Analysis on Large Scale Interacting Systems, RIMS Kôkyûroku Bessatsu, B59. Res. Inst. Math. Sci. (RIMS), Kyoto, pp. 101–115.
\newblock \burlalt{arXiv:1602.02428}{http://arxiv.org/abs/1602.02428}.

\bibitem{GP17}
M. ~Gubinelli, N. ~Perkowski.
\newblock {\em KPZ reloaded}.
\newblock  Comm. Math. Phys. \textbf{394}, no.~1, (2017), 165--269.
\newblock \burlalt{doi:10.1007/s00220-016-2788-3}{https://doi.org/10.1007/s00220-016-2788-3}.

\bibitem{GP18}
M.~Gubinelli, N.~Perkowski.
\newblock \emph{Energy solutions of KPZ are unique.} J. Amer. Math. Soc. (JAMS), \textbf{31}, no.~2, (2018),
  427-471.
\newblock \burlalt{doi:10.1090/jams/889}{http://dx.doi.org/10.1090/jams/889}. 

\bibitem{reg}
M. ~Hairer.
\newblock {\em A theory of regularity structures}.
\newblock Invent. Math. \textbf{198}, no.~2, (2014), 269--504.
\newblock
  \burlalt{doi:10.1007/s00222-014-0505-4}{https://dx.doi.org/10.1007/s00222-014-0505-4}.
  
  \bibitem{KH69}
K. ~Hepp.
\newblock {\em On the equivalence of additive and analytic renormalization}.
\newblock Comm. Math. Phys. \textbf{14}, (1969), 67--69.
\newblock \burlalt{doi:10.1007/
BF01645456}{http://dx.doi.org/10.1007/
BF01645456}.

\bibitem{HI67}
T. ~Hida and N. ~Ikeda.
\newblock {\em Analysis on Hilbert space with reproducing kernel
arising from multiple Wiener integral.} 
\newblock In Proc. Fifth Berkeley Sympos. Math. Statist. and Probability \textbf{2}, no.~1, (1967), 117-–143.

\bibitem{HM18} 
M. ~Hairer, K. ~Matetski.
\newblock {\em Discretisations of rough stochastic PDEs}.
\newblock {Ann. Probab. \textbf{46}, no.~3, (2018), 1651--1709.}
\burlalt{doi:10.1214/17-AOP1212}{https://doi.org/10.1214/17-AOP1212}.

\bibitem{HP15}
\newblock{M. ~Hairer, E. ~Pardoux}.
\newblock{\em A Wong-Zakai theorem for stochastic PDEs.}
\newblock{J. Math. Soc. Japan, \textbf{67}, no.~4, (2015), 1551--1604.}
\newblock \burlalt{doi:10.2969/jmsj/06741551}{https://doi.org/10.2969/jmsj/06741551}.
 
\bibitem{HQ15}
M. ~Hairer, J. ~Quastel.
\newblock {\em A class of growth models rescaling to KPZ}.
\newblock {Forum of Mathematics Pi, \textbf{6}, e3, (2018).}
\burlalt{doi:10.1017/fmp.2018.2}{https://doi.org/10.1017/fmp.2018.2}.


  



    	 


\bibitem{K16}
W. ~K\"{o}nig.
\newblock {\em The parabolic Anderson Model}.
\newblock Pathways in Mathematics \textbf{Xi}, (2016), 192.
\newblock \burlalt{doi:10.1007/978-3-319-33596-4}{https://doi.org/10.1007/978-3-319-33596-4}.


 
 \bibitem{LOT}
P.~Linares, F.~Otto, M.~Tempelmayr.
\newblock {\em The structure group for quasi-linear equations via universal enveloping algebras}. 
\newblock \burlalt{arXiv:2103.04187
}{https://arxiv.org/abs/2103.04187}. 
 

  \bibitem{LOTT}
P.~Linares, F.~Otto, M.~Tempelmayr, P.~Tsatsoulis.
\newblock {\em A diagram-free approach to the stochastic estimates in regularity structures}. 
\newblock \burlalt{arXiv:2112.10739
}{https://arxiv.org/abs/2112.10739}.


	
\bibitem{Mat18}
K. ~Matetski.
\newblock {\em Martingale-driven approximations of singular stochastic PDEs}.
\newblock \burlalt{arXiv:1808.09429}{https://arxiv.org/abs/1808.09429}.










\bibitem{MP19}
J. ~Martin, N. ~Perkowski.
\newblock {\em Paracontrolled distributions on Bravais lattices and weak universality of the 2d parabolic Anderson model}.
\newblock Ann. Inst. H. Poincaré Probab. Statist. \textbf{55}, no.~4, (2019), 2058--2110.
\newblock \burlalt{doi:10.1214/18-AIHP942}{https://doi.org/10.1214/18-AIHP942}.






\bibitem{MW17}
J. C. ~Mourrat, H. ~Weber.
\newblock {\em Convergence of the two-dimensional dynamic Ising-Kac model to $\Phi^4_2$}.
\newblock Comm. Pure Appl. Math. \textbf{70}, no.~4, (2017), 717--812.
\newblock \burlalt{doi:10.1002/cpa.21655}{https://doi.org/10.1002/cpa.21655}.












\bibitem{MY89}
P. A. ~Meyer, J. A. ~Yan.
\newblock {\em Distributions sur l\'{e}space de Wiener (suite) d\'{a}pr\`{e}s
I. Kubo et Y. Yokoi}.
\newblock S\'{e}minaire de Probabilit\'{e}s \textbf{XXIII}, no.~1372, (1989), 382-–392.

\bibitem{OSSW}
 F.~Otto, J.~Sauer, S.~Smith, H.~Weber.
\newblock {\em A priori bounds for quasi-linear SPDEs in the full sub-critical regime}. 
\newblock \burlalt{arXiv:2103.11039
}{https://arxiv.org/abs/2103.11039}.

\bibitem{Wick50}
G. C. ~Wick.
\newblock {\em The evaluation of the collision matrix}.
\newblock Physical Rev. \textbf{2}, no.~80, (1950), 268-–
272.
\newblock \burlalt{doi:10.1103/PhysRev.80.268}{https://doi.org/10.1103/PhysRev.80.268}
 

\bibitem{WZ69}
W. ~Zimmermann.
\newblock{\em Convergence of Bogoliubov’s method of renormalization in momentum space.}
\newblock {Comm. Math. Phys. \textbf{15}, (1969), 208--234.}
\newblock \burlalt{doi:10.1007/BF01645676}{http://dx.doi.org/10.1007/BF01645676}.
 

\bibitem{ZZ14}
R. ~Zhu, X. ~Zhu.
\newblock {\em Approximating three-dimensional Navier-Stokes equations driven by space-time white noise}.
\newblock Infinite Dimensional Analysis, Quantum Probability and Related Topics \textbf{20}, no.~4, (2017).
\newblock \burlalt{doi:10.1142/S0219025717500205}{https://doi.org/10.1142/S0219025717500205}.

\bibitem{ZZ15}
R. ~Zhu, X. ~Zhu.
\newblock {\em Piecewise linear approximations for dynamical $\varphi^4_3$ model}.
\newblock {Sci. China Math \textbf{63}, (2020), 381-–410.}
\newblock \burlalt{doi:10.1007/s11425-017-9269-1}{https://doi.org/10.1007/s11425-017-9269-1}.

\end{thebibliography}

\end{document}
