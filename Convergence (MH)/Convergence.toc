\contentsline {section}{\numberline {1}Introduction}{1}{section.1}%
\contentsline {section}{\numberline {2}Joint Cumulants}{2}{section.2}%
\contentsline {section}{\numberline {3}Labelled Graphs}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Directed MultiGraph and Contracted Graphs}{3}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Partition of Integral Domain}{6}{subsection.3.2}%
\contentsline {section}{\numberline {4}Elementary Labelled Graph}{11}{section.4}%
\contentsline {section}{\numberline {5}Solving the Generalised KPZ Equation}{16}{section.5}%
\contentsline {subsection}{\numberline {5.1}Discrete Model}{16}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Bounds on the Discrete Model}{16}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}General Criterion}{18}{subsubsection.5.2.1}%
\contentsline {subsubsection}{\numberline {5.2.2}Renormalisation Procedure}{20}{subsubsection.5.2.2}%
\contentsline {subsubsection}{\numberline {5.2.3}Pairing of Labelled Graphs}{25}{subsubsection.5.2.3}%
\contentsline {subsubsection}{\numberline {5.2.4}Example Computations}{29}{subsubsection.5.2.4}%
